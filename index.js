const { rot13 } = require('crypto-toolkit')

module.exports = require('./accounts.json').map(function(account) {
    return account.username + ':' + rot13(account.password);
});
