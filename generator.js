// TODO: USE LAST NAME FOR EMAILS eg: rui.ferrao@ecorp.com

var fs = require('fs');
var path = require('path');

var FILES = {
    USERNAME: path.join(__dirname, './assets/usernames.txt'),
    NAME: path.join(__dirname, './assets/names.txt'),
    PASSWORD: path.join(__dirname, './assets/password.txt'),
    COMBINED: path.join(__dirname, './accounts.json')
};

var TEXT_ENCODING = 'utf-8';

exports.ACC_TYPE = {
    USERNAME: 'username',
    EMAIL: 'email'
};

exports.generateAccounts = function(options, next) {

    var userStream, passwordStream, writer;
    var users, password, combined;

    options = options || {};
    options.type = options.type || exports.ACC_TYPE.USERNAME;
    options.amount = options.amount || 10;

    users = [];
    passwords = [];

    switch (options.type) {
        case exports.ACC_TYPE.USERNAME:
            userStream = fs.createReadStream(FILES.USERNAME, TEXT_ENCODING);
            break;
        case exports.ACC_TYPE.EMAIL:
            userStream = fs.createReadStream(FILES.NAME, TEXT_ENCODING);
            break;
        default:
            console.log('wrong account type. use one of the provided ACC_TYPE values');
            return;
    }

    passwordStream = fs.createReadStream(FILES.PASSWORD, TEXT_ENCODING);

    readStream(userStream, function(usersFile) {
        readStream(passwordStream, function(passwordsFile) {

            users = usersFile.toLowerCase().split('\n');
            passwords = passwordsFile.split('\n');
            combined = combine(users, passwords, options.amount, options.domain);

            writer = fs.createWriteStream(FILES.COMBINED);
            writer.write(JSON.stringify(combined));
            writer.end();

            //while the writer ends writing to the file, 'return' the combined object write away..
            if (next) {
                next(combined);
            }
        });
    });
};

function readStream(stream, next) {

    var fileContents = '';

    // if this has more than 10k entries (per example) will this have an affect on performance? how big should the string get?
    stream.on('data', function(data) {
        fileContents += data;
    });

    stream.on('end', function() {
        next(fileContents);
    });

}

function combine(usernames, passwords, amount, domain) {

    //HACK: files end with newline
    var newLine = 1;

    if (amount > usernames.length - newLine) {
        throw 'amount of accounts desired is bigger than available usernames';
    }

    if (amount > passwords.length - newLine) {
        throw 'amount of accounts desired is bigger than available passwords';
    }

    var randomUser, randomPassword, account;
    var combined = [];

    while (amount > 0) {
        randomUser = Math.floor(Math.random() * (usernames.length - newLine));
        randomPassword = Math.floor(Math.random() * (passwords.length - newLine));

        account = {
            username: usernames.splice(randomUser, 1)[0],
            password: passwords.splice(randomPassword, 1)[0]
        };

        if (domain) {
            account.username += '@' + domain;
        }

        combined.push(account);
        amount--;
    }

    return combined;

}
