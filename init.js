var generator = require('./generator');

module.exports = function() {
    generator.generateAccounts({
        amount: 20,
        type: generator.ACC_TYPE.EMAIL,
        domain: 'ecorp.com'
    });
};
